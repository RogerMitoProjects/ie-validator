#include <stdbool.h>
#include <string.h>
#include <ctype.h>

int const weights[12] = {5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
int firstDigit = 0, secondDigit = 0;

bool ac(char *ie)
{
    int ieDigits[strlen(ie)];

    if (strlen(ie) != 13)
        return false;
    if (getDigits(ieDigits, ie))
    {
        if (ieDigits[0] != 0 || ieDigits[1] != 1)
            return false;

        for (int i = 1; i <= 11; i++)
        {
            firstDigit += ieDigits[i - 1] * weights[i];
            secondDigit += ieDigits[i - 1] * weights[i - 1];
        }
        secondDigit += ieDigits[11] * weights[11];

        firstDigit %= 11;
        secondDigit %= 11;

        firstDigit = 11 - firstDigit > 9 ? 0 : 11 - firstDigit;
        secondDigit = 11 - secondDigit > 9 ? 0 : 11 - secondDigit;
        return firstDigit == ieDigits[11] && secondDigit == ieDigits[12];
    }
    return false;
}
bool al(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        if (ieDigits[0] != 2 || ieDigits[1] != 4)
            return false;

        for (int i = 4; i < 12; i++)
        {
            firstDigit += ieDigits[i - 4] * weights[i];
        }
        firstDigit *= 10;
        firstDigit %= 11;
        return (firstDigit == 10 ? 0 : firstDigit) == ieDigits[8];
    }
    return false;
}
bool ap(char *ie)
{
    int ieDigits[strlen(ie)];
    if (getDigits(ieDigits, ie))
    {
        int p = 0, d = 0;
        if (strlen(ie) != 9)
            return false;
        if (ieDigits[0] != 0 || ieDigits[1] != 3)
            return false;
        char aux[9];
        memset(aux, '\0', sizeof(aux));
        strncpy(aux, ie, 8);
        int range = atoi(aux);

        if (range >= 3000001 && range <= 3017000)
        {
            p = 5;
        }

        else if (range >= 3017001 && range <= 3019022)
        {
            d = 1;
            p = 9;
        }

        for (int i = 4; i < 12; i++)
        {
            firstDigit += ieDigits[i - 4] * weights[i];
        }
        firstDigit += p;
        firstDigit = 11 - (firstDigit % 11);
        if (firstDigit == 10)
            firstDigit = 0;
        else if (firstDigit == 11)
            firstDigit = 11;
        return firstDigit == ieDigits[8];
    }
    return false;
}
bool am(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        for (int i = 4; i < 12; i++)
        {
            firstDigit += ieDigits[i - 4] * weights[i];
        }
        if (firstDigit < 11)
        {
            firstDigit = 11 - firstDigit;
        }
        else
        {
            firstDigit %= 11;
            firstDigit = firstDigit < 2 ? 0 : 11 - firstDigit;
        }

        return firstDigit == ieDigits[8];
    }
    return false;
}
bool ba(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) == 8 || strlen(ie) == 9)
        if (getDigits(ieDigits, ie))
        {
            if (strlen(ie) == 8)
            {
                for (int i = 6; i < 12; i++)
                {
                    secondDigit += ieDigits[i - 6] * weights[i];
                    firstDigit += ieDigits[i - 6] * weights[i - 1];
                }
                // Módulo 11
                if (ieDigits[0] == 6 || ieDigits[0] == 7 || ieDigits[0] == 9)
                {
                    secondDigit = (secondDigit % 11) == 0 || (secondDigit % 11) == 1 ? 0 : 11 - (secondDigit % 11);
                    firstDigit += weights[11] * secondDigit;
                    firstDigit = 11 - firstDigit % 11;
                }
                // Módulo 10
                else
                {
                    secondDigit = secondDigit % 10 == 0 ? 0 : 10 - secondDigit % 10;
                    firstDigit += weights[11] * secondDigit;
                    firstDigit = 10 - firstDigit % 10;
                }
                return firstDigit == ieDigits[6] && secondDigit == ieDigits[7];
            }
            // Cálculo para ie com 9 dígitos
            else
            {
                for (int i = 5; i < 12; i++)
                {
                    secondDigit += ieDigits[i - 5] * weights[i];
                    firstDigit += ieDigits[i - 5] * weights[i - 1];
                }

                if (ieDigits[1] == 6 || ieDigits[1] == 7 || ieDigits[1] == 9)
                {
                    secondDigit = secondDigit % 11 == 0 || secondDigit % 11 == 1 ? 0 : 11 - secondDigit % 11;
                    firstDigit += weights[11] * secondDigit;
                    firstDigit = firstDigit % 11 == 0 || firstDigit % 11 == 1 ? 0 : 11 - firstDigit % 11;
                }
                else
                {
                    secondDigit = secondDigit % 10 == 0 ? 0 : 10 - secondDigit % 10;
                    firstDigit += weights[11] * secondDigit;
                    firstDigit = firstDigit % 10 == 0 ? 0 : 10 - firstDigit % 10;
                }

                return firstDigit == ieDigits[7] && secondDigit == ieDigits[8];
            }
        }
    return false;
}
bool ce(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        for (int i = 0; i < 8; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 4];
        }
        firstDigit %= 11;
        firstDigit = (11 - firstDigit) >= 10 ? 0 : 11 - firstDigit;
        return firstDigit == ieDigits[8];
    }
    return false;
}
bool df(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 13)
        return false;
    if (getDigits(ieDigits, ie))
    {
        for (int i = 0; i < 11; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 1];
            secondDigit += ieDigits[i] * weights[i];
        }
        firstDigit = (11 - firstDigit % 11 == 10) || (11 - firstDigit % 11 == 11) ? 0 : 11 - firstDigit % 11;
        secondDigit += firstDigit * weights[11];
        secondDigit = (11 - secondDigit % 11 == 10) || (11 - secondDigit % 11 == 11) ? 0 : 11 - secondDigit % 11;
        return firstDigit == ieDigits[11] && secondDigit == ieDigits[12];
    }
    return false;
}
bool es(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        for (int i = 0; i < 8; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 4];
        }
        firstDigit %= 11;
        firstDigit = firstDigit < 2 ? 0 : 11 - firstDigit;
        return firstDigit == ieDigits[8];
    }
    return false;
}
bool go(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        if (strcmp("110944021", ie) == 0 || strcmp("110944020", ie) == 0)
        {
            return ieDigits[8] == 1 || ieDigits[8] == 0;
        }
        if (ieDigits[0] == 1 && (ieDigits[1] == 0 || ieDigits[1] == 5 || ieDigits[1] == 1))
        {
            for (int i = 0; i < 8; i++)
            {
                firstDigit += ieDigits[i] * weights[i + 4];
            }
            int rest = firstDigit %= 11;
            char aux[9];
            memset(aux, '\0', sizeof(aux));
            strncpy(aux, ie, 8);
            int range = atoi(aux);
            if (rest == 1 && (range >= 10103105) && range <= 10119997)
                firstDigit = 1;
            else if (rest == 1)
                firstDigit = 0;
            else if (rest == 0)
                firstDigit = 0;
            else
                firstDigit = 11 - rest;
            return firstDigit == ieDigits[8];
        }
        return false;
    }
    return false;
}
bool ma(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        for (int i = 0; i < 8; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 4];
        }
        firstDigit %= 11;
        firstDigit = firstDigit <= 1 ? 0 : 11 - firstDigit;
        return firstDigit == ieDigits[8];
    }
    return false;
}
bool mt(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 11)
        return false;

    if (getDigits(ieDigits, ie))
    {
        for (int i = 0; i < 11; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 2];
        }
        firstDigit %= 11;
        firstDigit = firstDigit <= 1 ? 0 : 11 - firstDigit;

        return firstDigit == ieDigits[10];
    }
    
    return false;
}
bool ms(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        if (ieDigits[0] == 2 && ieDigits[1] == 8)
        {
            for (int i = 0; i < 8; i++)
            {
                firstDigit += ieDigits[i] * weights[i + 4];
            }
            int rest = firstDigit %= 11;
            if (rest == 0)
                firstDigit = 0;
            else if (11 - rest > 9)
                firstDigit = 0;
            else if (11 - rest < 10)
                firstDigit = 11 - rest;

            return firstDigit == ieDigits[8];
        }
        return false;
    }
    return false;
}
bool mg(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 13)
        return false;

    if (getDigits(ieDigits, ie))
    {
        int sum = 0, digit;
        /*
                Igualar as casas para o cálculo, o que consiste em inserir o algarismo zero "0"
                imediatamente após o número de código do município, desprezando-se os dígitos de controle.
            */
        // Array auxilixar para comportar a inserção do dígito 0.
        int aux[12];
        aux[3] = 0;
        for (int i = 0, j = 0; i < 12; i++, j++)
        {
            // Compensação do 0 inserido na posição 3
            if (j == 3)
            {
                i++;
            }
            aux[i] = ieDigits[j];
            if (i % 2 != 0)
                aux[i] *= 2;
        }
        for (int i = 0; i < 12; i++)
        {
            digit = aux[i];

            if (aux[i] > 9)
            {
                digit = aux[i] % 10;
                sum += digit;
                aux[i] /= 10;
                digit = aux[i] % 10;
                sum += digit;
            }
            else
            {
                digit = aux[i];
                sum += digit;
            }
        }
        if (sum % 10 != 0)
        {
            firstDigit = ((10 - sum % 10) + sum) - sum;
        }
        else
            firstDigit = 0;

        // Segundo dígito

        //Pesos específicos para o estado de MG
        int const mgWeights[12] = {3, 2, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
        for (int i = 0; i < 12; i++)
        {
            secondDigit += mgWeights[i] * ieDigits[i];
        }
        secondDigit %= 11;
        secondDigit = secondDigit <= 1 ? 0 : 11 - secondDigit;
        return firstDigit == ieDigits[11] && secondDigit == ieDigits[12];
    }
    return false;
}
bool pa(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        if (ieDigits[0] == 1 && ieDigits[1] == 5)
        {
            for (int i = 0; i < 8; i++)
            {
                firstDigit += ieDigits[i] * weights[i + 4];
            }
            firstDigit %= 11;
            firstDigit = firstDigit <= 1 ? 0 : 11 - firstDigit;
            return firstDigit == ieDigits[8];
        }
        return false;
    }
    return false;
}
bool pb(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        for (int i = 0; i < 8; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 4];
        }

        firstDigit %= 11;
        firstDigit = firstDigit <= 1 ? 0 : 11 - firstDigit;
        return firstDigit == ieDigits[8];
    }
    return false;
}
bool pr(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 10)
        return false;
    if (getDigits(ieDigits, ie))
    {
        //Pesos específicos para o estado do PR
        int const prWeights[9] = {4, 3, 2, 7, 6, 5, 4, 3, 2};
        for (int i = 0; i < 8; i++)
        {
            firstDigit += ieDigits[i] * prWeights[i + 1];
            secondDigit += ieDigits[i] * prWeights[i];
        }
        firstDigit %= 11;
        firstDigit = firstDigit <= 1 ? 0 : 11 - firstDigit;
        secondDigit += prWeights[8] * firstDigit;
        secondDigit %= 11;
        secondDigit = secondDigit <= 1 ? 0 : 11 - secondDigit;
        return firstDigit == ieDigits[8] && secondDigit == ieDigits[9];
    }
    return false;
}
bool pe(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {

        for (int i = 0; i < 7; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 5];
            secondDigit += ieDigits[i] * weights[i + 4];
        }
        firstDigit %= 11;
        firstDigit = firstDigit <= 1 ? 0 : 11 - firstDigit;
        secondDigit += weights[11] * firstDigit;
        secondDigit %= 11;
        secondDigit = secondDigit <= 1 ? 0 : 11 - secondDigit;
        return firstDigit == ieDigits[7] && secondDigit == ieDigits[8];
    }
    return false;
}
bool pi(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {
        for (int i = 0; i < 8; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 4];
        }
        firstDigit %= 11;
        firstDigit = firstDigit <= 1 ? 0 : 11 - firstDigit;

        return firstDigit == ieDigits[8];
    }
    return false;
}
bool rj(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 8)
        return false;
    if (getDigits(ieDigits, ie))
    {
        for (int i = 0; i < 6; i++)
        {
            firstDigit += ieDigits[i + 1] * weights[i + 6];
        }
        firstDigit += ieDigits[0] * 2;
        firstDigit %= 11;
        firstDigit = firstDigit <= 1 ? 0 : 11 - firstDigit;
        return firstDigit == ieDigits[7];
    }
    return false;
}
bool rn(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9 && strlen(ie) != 10)
        return false;
    if (getDigits(ieDigits, ie))
    {
        if (ieDigits[0] == 2 && ieDigits[1] == 0)
        {
            if (strlen(ie) == 9)
            {
                for (int i = 0; i < 8; i++)
                {
                    firstDigit += ieDigits[i] * weights[i + 4];
                }
                firstDigit *= 10;
                firstDigit %= 11;
                firstDigit = firstDigit == 10 ? 0 : firstDigit;
                return firstDigit == ieDigits[8];
            }
            for (int i = 0; i < 8; i++)
            {
                firstDigit += ieDigits[i + 1] * weights[i + 4];
            }
            firstDigit += firstDigit * 10;
            firstDigit *= 10;
            firstDigit %= 11;
            firstDigit = firstDigit == 10 ? 0 : firstDigit;
            return firstDigit == ieDigits[9];
        }
        return false;
    }
    return false;
}
bool rs(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 10)
        return false;
    if (getDigits(ieDigits, ie))
    {
        for (int i = 0; i < 9; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 3];
        }
        firstDigit %= 11;
        firstDigit = firstDigit >= 10 ? 0 : 11 - firstDigit;
        return firstDigit == ieDigits[9];
    }
    return false;
}
bool ro(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9 && strlen(ie) != 14)
        return false;
    if (getDigits(ieDigits, ie))
    {
        if (strlen(ie) == 9)
        {
            for (int i = 0; i < 5; i++)
            {
                firstDigit += ieDigits[i + 3] * weights[i + 7];
            }
            firstDigit %= 11;
            firstDigit = firstDigit >= 10 ? firstDigit - 10 : 11 - firstDigit;
            return firstDigit == ieDigits[8];
        }
        for (int i = 0, j = 7; i < 13; i++, j++)
        {
            if (i == 5)
                j = 4;

            firstDigit += ieDigits[i] * weights[j];
        }
        firstDigit %= 11;
        firstDigit = firstDigit >= 10 ? firstDigit - 10 : 11 - firstDigit;
        return firstDigit == ieDigits[13];
    }
    return false;
}
bool rr(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {

        for (int i = 0; i < 8; i++)
        {
            firstDigit += ieDigits[i] * (i + 1);
        }
        firstDigit %= 9;
        return firstDigit == ieDigits[8];
    }
    return false;
}
bool sc(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {

        for (int i = 0; i < 8; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 4];
        }
        firstDigit %= 11;
        firstDigit = 11 - firstDigit <= 1 ? 0 : 11 - firstDigit;
        return firstDigit == ieDigits[8];
    }
    return false;
}
bool sp(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) == 12 || strlen(ie) == 13)
    {
        //Pesos específicos para o estado de SP (primeiro dígito)
        int const spFirstDigitWeights[8] = {1, 3, 4, 5, 6, 7, 8, 10};

        //Pesos específicos para o estado de SP (segundo dígito)
        int const spSecondDigitWeights[11] = {3, 2, 10, 9, 8, 7, 6, 5, 4, 3, 2};

        // Validação para IE de produtores rurais do estado de SP
        if (ie[0] == 'P')
        {
            char aux[12];
            memset(aux, '\0', sizeof(aux));
            strncpy(aux, ie + 1, 12);
            ie = aux;
            if (getDigits(ieDigits, ie))
            {
                for (int i = 0; i < 8; i++)
                {
                    firstDigit += ieDigits[i] * spFirstDigitWeights[i];
                }
                firstDigit %= 11;
                firstDigit %= 10;
                return firstDigit == ieDigits[8];
            }
            return false;
        }

        if (getDigits(ieDigits, ie))
        {

            for (int i = 0; i < 8; i++)
            {
                firstDigit += ieDigits[i] * spFirstDigitWeights[i];
            }

            for (int i = 0; i < 11; i++)
            {
                secondDigit += ieDigits[i] * spSecondDigitWeights[i];
            }
            firstDigit %= 11;
            // O primeiro dígito verificador será o algarismo mais à direita do resto da divisão do resultado acima
            firstDigit %= 10;
            secondDigit %= 11;
            // O segundo dígito verificador será o algarismo mais à direita do resto da divisão do resultado acima
            secondDigit %= 10;
            return firstDigit == ieDigits[8] && secondDigit == ieDigits[11];
        }
    }

    return false;
}
bool se(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 9)
        return false;
    if (getDigits(ieDigits, ie))
    {

        for (int i = 0; i < 8; i++)
        {
            firstDigit += ieDigits[i] * weights[i + 4];
        }
        firstDigit %= 11;
        firstDigit = 11 - firstDigit >= 10 ? 0 : 11 - firstDigit;
        return firstDigit == ieDigits[8];
    }
    return false;
}
bool to(char *ie)
{
    int ieDigits[strlen(ie)];
    if (strlen(ie) != 11)
        return false;

    if (getDigits(ieDigits, ie))
    {

        for (int i = 0; i < 6; i++)
        {
            firstDigit += ieDigits[i + 4] * weights[i + 6];
        }
        firstDigit += ieDigits[0] * weights[4];
        firstDigit += ieDigits[1] * weights[5];
        firstDigit %= 11;
        firstDigit = firstDigit < 2 ? 0 : 11 - firstDigit;
        return firstDigit == ieDigits[10];
    }
    return false;
}