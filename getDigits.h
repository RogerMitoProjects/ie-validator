bool getDigits(int *ieDigits, char ie[]) {
    char x;
    int length = strlen(ie);

    for (int i = 0; i < length; i++) {
        if (!isdigit(ie[i]))
            return false;

        x = ie[i];
        ieDigits[i] = atoi(&x);
    }

    return true;
}