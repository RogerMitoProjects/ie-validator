#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "getDigits.h"
#include "ie.h"

int ufToInt(char *uf)
{
    char ufSet[27][3] = {"AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
    for (int i = 0; i < 28; i++)
    {
        if (strcmp(uf, ufSet[i]) == 0)
            return i;
    }
    return -1;
}

bool validate(char uf[3], char *ie)
{
    int index = ufToInt(uf);
    if (index >= 0)
    {
        bool (*states[28])(char *ie) = {ac, al, ap, am, ba, ce, df, es, go, ma, mt, ms, mg, pa, pb, pr, pe, pi, rj, rn, rs, ro, rr, sc, sp, se, to};
        return (*states[index])(ie);
    }
    return false;
}

int main(int argc, char const *argv[])
{
    
    char *uf = "MT";
    char *ie= "17080033410";
    printf("%s", (validate(uf, ie) == true ? "Valid IE" : "Invalid IE"));
    return 0;
}
